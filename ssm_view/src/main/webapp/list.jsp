<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta charset="UTF-8">
    <title>欢迎页面-X-admin2.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="./css/font.css">
    <link rel="stylesheet" href="./css/xadmin.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="./js/xadmin.js"></script>
    
    <!-- 新增 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- 声明当前网页在移动端浏览器中展示的相关设置 -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
		<title>BootstrapTable绘制工具栏(ToolBar)</title>
		<!-- 引入Bootstrap核心样式表文件 -->
		<link rel="stylesheet" href="css/bootstrap.css">
		<!-- 引入Table核心样式表文件 -->
		<link rel="stylesheet" href="css/bootstrap-table.css" />
		<!-- html5shiv让浏览器可以识别HTML5的新标签 -->
		<!-- respond让低版本浏览器可以使用CSS3的媒体查询 -->
		<!--[if lt IE 9]>
      <script src="lib/html5shiv/html5shiv.min.js"></script>
      <script src="lib/respond/respond.min.js"></script>
    <![endif]-->
		<!-- 自己写的文件默认放在最下面 -->
		<!--<link rel="stylesheet" href="css/main.css">-->
		<!-- Bootstrap 的所有JS组件都是依赖JQUERY的 -->
		<script src="js/jquery-1.12.0.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.js"></script>
		<!-- Table表格插件-->
		<script src="js/bootstrap-table.js"></script>
		<!--中文编码处理-->
		<script src="js/bootstrap-table-zh-CN.js"></script>
</head>
<body>
 <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a href="">演示</a>
        <a>
          <cite>导航元素</cite></a>
      </span>
      <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
        <i class="layui-icon" style="line-height:30px">ဂ</i></a>
    </div>
    <div class="x-body">
      <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so">
          <input class="layui-input" placeholder="开始日" name="start" id="start">
          <input class="layui-input" placeholder="截止日" name="end" id="end">
          <input type="text" name="username"  placeholder="请输入用户名" autocomplete="off" class="layui-input">
          <button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon">&#xe615;</i></button>
        </form>
      </div>
      <xblock>
        <button class="layui-btn layui-btn-danger" onclick="delAll()"><i class="layui-icon"></i>批量删除</button>
        <button class="layui-btn" onclick="x_admin_show('添加用户','./admin-add.html')"><i class="layui-icon"></i>添加</button>
        <span class="x-right" style="line-height:40px">共有数据：88 条</span>
      </xblock>
      <table class="layui-table" id="tradeList">
       	<thead>
       		<tr>
					<th data-field="custId">客户编号</th>
					<th data-field="custName">客户姓名</th>
					<th data-field="custMobile">联系方式</th>
					<th data-field="custEmail">电子邮箱</th>
					<th data-field="custAddress">地址</th>
       		</tr>
       	</thead>
      </table>
    </div>
    <script>
      layui.use('laydate', function(){
        var laydate = layui.laydate;
        
        //执行一个laydate实例
        laydate.render({
          elem: '#start' //指定元素
        });

        //执行一个laydate实例
        laydate.render({
          elem: '#end' //指定元素
        });
      });

       /*用户-停用*/
      function member_stop(obj,id){
          layer.confirm('确认要停用吗？',function(index){

              if($(obj).attr('title')=='启用'){

                //发异步把用户状态进行更改
                $(obj).attr('title','停用')
                $(obj).find('i').html('&#xe62f;');

                $(obj).parents("tr").find(".td-status").find('span').addClass('layui-btn-disabled').html('已停用');
                layer.msg('已停用!',{icon: 5,time:1000});

              }else{
                $(obj).attr('title','启用')
                $(obj).find('i').html('&#xe601;');

                $(obj).parents("tr").find(".td-status").find('span').removeClass('layui-btn-disabled').html('已启用');
                layer.msg('已启用!',{icon: 5,time:1000});
              }
              
          });
      }

      /*用户-删除*/
      function member_del(obj,id){
          layer.confirm('确认要删除吗？',function(index){
              //发异步删除数据
              $(obj).parents("tr").remove();
              layer.msg('已删除!',{icon:1,time:1000});
          });
      }



      function delAll (argument) {

        var data = tableCheck.getData();
  
        layer.confirm('确认要删除吗？'+data,function(index){
            //捉到所有被选中的，发异步进行删除
            layer.msg('删除成功', {icon: 1});
            $(".layui-form-checked").not('.header').parents('tr').remove();
        });
      }
    </script>
    <script>var _hmt = _hmt || []; (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
      })();</script>
</body>

<script type="text/javascript">

$(function () {
	/*数据json*/
	var json = [{
			"custId": "1",
			"custName": "刘德华",
			"custMobile": "134543212345",
			"custEmail": "andy@qq.com",
			"custAddress": "xxxxx"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		},
		{
			"custId": "2",
			"custName": "aaa",
			"custMobile": "134543212344",
			"custEmail": "dddd@qq.com",
			"custAddress": "ddddddd"
		}
	];
	//第二步：使用组件函数bootstrapTable加载JSON对象
	//JavaScript加载JSON
	$("#tradeList").bootstrapTable({
		data: json,
		pagination : true,
        pageNumber:1,                       //初始化加载第一页，默认第一页
        pageSize: 5,                       //每页的记录行数（*）
        pageList: [5, 10, 25, 50]        //可供选择的每页的行数（*）
	});
	
	
    //1.初始化Table
/*     var oTable = new TableInit();
    oTable.Init(); */

    //2.初始化Button的点击事件
    /* var oButtonInit = new ButtonInit();
    oButtonInit.Init(); */

});


/* var TableInit = function () {
    var oTableInit = new Object();
    //初始化Table
    oTableInit.Init = function () {
        $('#tradeList').bootstrapTable({
            url: '/VenderManager/TradeList',         //请求后台的URL（*）
            method: 'post',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            queryParams: oTableInit.queryParams,//传递参数（*）
            sidePagination: "client",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 50,                       //每页的记录行数（*）
            pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
            strictSearch: true,
            clickToSelect: true,                //是否启用点击选中行
            height: 460,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "id",                     //每一行的唯一标识，一般为主键列
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [{
                field: 'id',
                title: '序号'
            }, {
                field: 'liushuiid',
                title: '交易编号'
            }, {
                field: 'orderid',
                title: '订单号'
            }, {
                field: 'receivetime',
                title: '交易时间'
            }, {
                field: 'price',
                title: '金额'
            },]
        });
    };

    //得到查询的参数
  oTableInit.queryParams = function (params) {
        var temp = {   //这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            limit: params.limit,   //页面大小
            offset: params.offset,  //页码
            sdate: $("#stratTime").val(),
            edate: $("#endTime").val(),
            sellerid: $("#sellerid").val(),
            orderid: $("#orderid").val(),
            CardNumber: $("#CardNumber").val(),
            maxrows: params.limit,
            pageindex:params.pageNumber,
            portid: $("#portid").val(),
            CardNumber: $("#CardNumber").val(),
            tradetype:$('input:radio[name="tradetype"]:checked').val(),
            success:$('input:radio[name="success"]:checked').val(),
        };
        return temp;
    };
    return oTableInit;
}; */

</script>
</html>