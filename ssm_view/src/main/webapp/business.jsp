<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<title>欢迎页面-X-admin2.0</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="./css/font.css">
<link rel="stylesheet" href="./css/xadmin.css">
<script type="text/javascript"
	src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="./lib/layui/layui.js"
	charset="utf-8"></script>
<script type="text/javascript" src="./js/xadmin.js"></script>

<!-- 新增 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- 声明当前网页在移动端浏览器中展示的相关设置 -->
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
<title>BootstrapTable绘制工具栏(ToolBar)</title>
<!-- 引入Bootstrap核心样式表文件 -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- 引入Table核心样式表文件 -->
<link rel="stylesheet" href="css/bootstrap-table.css" />
<!-- html5shiv让浏览器可以识别HTML5的新标签 -->
<!-- respond让低版本浏览器可以使用CSS3的媒体查询 -->
<!--[if lt IE 9]>
      <script src="lib/html5shiv/html5shiv.min.js"></script>
      <script src="lib/respond/respond.min.js"></script>
    <![endif]-->
<!-- 自己写的文件默认放在最下面 -->
<!--<link rel="stylesheet" href="css/main.css">-->
<!-- Bootstrap 的所有JS组件都是依赖JQUERY的 -->
<script src="js/jquery-1.12.0.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<!-- Table表格插件-->
<script src="js/bootstrap-table.js"></script>
<!--中文编码处理-->
<script src="js/bootstrap-table-zh-CN.js"></script>
</head>
<body>
	<div class="x-nav"></div>
	<div class="x-body">
		<xblock>
		<button class="layui-btn layui-btn-xs"
			onclick="x_admin_show('保存','./xxx.html')">
			<i class="layui-icon">保存</i>
		</button>
		<button class="btn btn-xs btn-primary" onclick="delAll()">
			<i class="layui-icon">&#xe642;工单操作</i>
		</button>
		</xblock>
		<!-- <form class="layui-form" action="">
			<div class="layui-row">
				<div class="layui-form-item layui-col-md6">
					<label class="layui-form-label">来电号码:</label>
					<div class="layui-input-block">
						<input type="text" name="phone" required lay-verify="required"
							placeholder="请输入号码" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item layui-col-md6">
					<label class="layui-form-label">客户编号:</label>
					<div class="layui-input-block">
						<input type="text" name="xxx" required lay-verify="required"
							placeholder="请输入密码" autocomplete="off" class="layui-input">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
					<button type="reset" class="layui-btn layui-btn-primary">重置</button>
				</div>
			</div>
		</form>
	</div> -->

		<div class="row">
			<div class=" col-md-6">
				<div class="col-md-2">
					<label style="margin-top: 5px">来电号码:</label>
				</div>
				<div class="col-md-6">
					<input type="text" class="form-control" />
				</div>
				<div class=" col-md-offset-4"></div>

			</div>
		</div>

		<div class="col-lg-6">
			<label class="layui-form-label">客户编号:</label> <input type="text"
				class="form-control" placeholder="请输入客户编号">
		</div>
</body>
</html>